import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

style.use('fivethirtyeight')

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

def run():
    plt.plot([1, 2, 3], [2, 2, 5])
    plt.show()


def animate(i): # i is interval
    # get xs and ys values from the source
    xs = [1, 2, 3, 4, 5, 6, 7]
    ys = [0, 8, 3, 9, 1, 5, 9,]
    ax1.clear()
    ax1.plot(xs, ys)
    plt.show(block=False)

def do_animation():
    ani = animation.FuncAnimation(fig, animate, interval=1000)
    plt.show()

