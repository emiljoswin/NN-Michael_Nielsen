# Trying my hand once more at this. I has almost quit doing this, before the
# sudden revival of interest.

import numpy as np
import random
import datetime
import matplotlib.pyplot as plt
from matplotlib import style

style.use('fivethirtyeight')

plt.ion()
plt.xlabel('Epochs')
plt.ylabel("Cost")

class Network(object):
    
    def __init__(self, sizes):
        #understood
        self.num_layers = len(sizes)
        self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
        self.weights = [ np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:]) ]

    def SGD(self, training_data, epochs, mini_batch_size, eta, test_data=None):
        '''
            Pick mini batches of data and update the weight vectors based on back prop done on
            each example in the mini batch.
        '''
        if test_data:
            n_test = len(test_data)

        a, b = 0, 0
        epoch_list, costs = [], []
        for j in xrange(epochs):
            a = datetime.datetime.now()
            random.shuffle(training_data)
            mini_batches = [ training_data[k: k + mini_batch_size] for k in xrange(0, len(training_data), mini_batch_size) ]

            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, eta)

            b = datetime.datetime.now()
            print(b - a)
            a = b
            if test_data:
               no_of_correct_answers = self.evaluate(test_data)
               print("Epoch %d completed with test data. Score %d / %d " %(j, no_of_correct_answers, n_test))
            else:
               print("Epoch %d done" %j)

            epoch_list.append(j)
            costs.append(no_of_correct_answers/float(n_test))
            plt.clf()
            plt.plot(epoch_list, costs)
            plt.pause(1.05)

        plt.title("30 Epochs with 100 Hidden Neurons", fontname='Ubuntu', fontsize=14, fontweight='bold', color='green')
        plt.savefig('30epochs100hidden_sigmoid_cross_entropy.png')
        #while True:
        #    plt.pause(0.001)


    def update_mini_batch(self, mini_batch, eta):
        '''
            Update the full weight matrix for the network using one mini batch.
            Note that the gradients are summed up and only after the entire minibatch is
            returns the gradients, do we actually reduce the weights and biases
        '''
        #initialize zero arrays to keep the sum of the gradients returned from backprop.
        nabla_b = [ np.zeros(b.shape) for b in self.biases ]
        nabla_w = [ np.zeros(w.shape) for w in self.biases ]

        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)


            nabla_b = [ nb + dnb for nb, dnb in zip(nabla_b, delta_nabla_b) ]
            nabla_w = [ nw + dnw for nw, dnw in zip(nabla_w, delta_nabla_w) ]

        # understood well. We are just doing w = w - eta*gradient.
        # It is however, done in this form because we are doing it for a bunch of training data
        # inside the mini_batch and not just one dealing with one single gradient.
        # We are summing the gradient for all the examples inside the minibatch
        self.weights = [ w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_w) ]
        self.biases= [ b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_b) ]

    def backprop(self, x, y):
        nabla_b = [ np.zeros(b.shape) for b in self.biases ]
        nabla_w = [ np.zeros(w.shape) for w in self.weights ]

        # 1. forward pass
        activation = x
        activations = [x] #in first layer the input is the activation
        zs = [] #array to store all z vectors, layer by layer

        #for w, b in zip(self.weights, self.biases):
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)

        # 2. Compute the gradient of the cost
        # NOTE: Need not use a funciton to calculate derivative of cross entropy function, Instead just omit sigmoid_prime.
        # it is going to get cancelled in the equation.
        delta = self.cost_derivative(activations[-1], y)

        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose()) #activations[-1] is the output of network, ie, network's y, ie, the networks last layer.

        # 3. Backward pass
        # l is used to index from the back of the layer by going -l, -l-1, -l-2 etc 
        for l in xrange(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())

        return (nabla_b, nabla_w)

    def cost_derivative(self, output_activations, y):
        '''
            cost derivative of quadratic cost
        '''
        return (output_activations - y)

    def cost_derivative_cross_entropy(self, output_activations, y):
        '''
            Cost derivative of cross entropy cost.
            If we use this cost function, we can make the learning speed proportional
            to the magnitude of the error ie, the more wrong we are the faster we learn
            to correct it. That is how humans learn, we realize the wrongness faster when we are 
            more wrong at something rather than being less wrong at it. Sigmoid activation  with
            cross entropy cost has this problem and the reason is stated below.

            The error that gets calculated at the output layer is asymptoted by the derivative 
            of the activation function like this:

                delta = derivative of C wrt a * derivative of a wrt z

            What happens here is the activation saturates quickly to both positive and negative ends (property of sigmoid function - check its graph).
            Hence, the derivative will be close to zero and delta will be close to zero and therefore
            the learning happens really slowly.
            It is quite clever how this can be mitigated. My first impression was to change the activation
            function to something that has different properties. 
            Instead what they do is change the cost function in such a way that that some parts of the 
            derivative of the Cost wrt a will cancel out the derivative of activation function entirely.
                
                delta = "some term that doesnt have derivative of activation function"

            The cross entropy function when differentiated will have a denominator equal to the
            derivative of the objective function and they gets cancelled out.
        '''
        return (output_activations - y) / (output_activations * (1 - output_activations))

    def feedforward(self, a):
        '''
            given the input a, this function outputs the networks output, ie, the network's y
            with its biases and weights. Usually called after the learning is completed.
        '''
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a) + b)
        return a

    def evaluate(self, test_data):
        '''
            test data contains x and y,
            Evaluate the output from the network (after the network has learned)
        '''
        #test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in test_data]
        #return sum(int(x == y) for (x, y) in test_results)
        success = 0
        for x, y in test_data:
            network_output = np.argmax(self.feedforward(x))
            if y == network_output:
                success += 1
        return success
                


def sigmoid(z):
    return 1.0 / ( 1 + np.exp(-z) )

def sigmoid_prime(z):
    return sigmoid(z) * ( 1 - sigmoid(z) )
