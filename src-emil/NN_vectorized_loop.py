# Trying my hand once more at this. I has almost quit doing this, before the
# sudden revival of interest.

## TODO - 25/09/2017, 
## 1. convert the backprop through layers into a loop. This makes it much more readable
## 2. Have different classes for different cost functions, a) quadartic cost, b) cross entropy, c) loglikelihood
## 3. Have a mechanism that will enable the plugging in of different activaion functions for different layers.

import numpy as np
import random
import datetime
import matplotlib.pyplot as plt
from matplotlib import style

style.use('fivethirtyeight')

plt.ion()
plt.xlabel('Epochs')
plt.ylabel("Cost")


class QuadraticCost(object):

    @staticmethod
    def name():
        return "Quadratic Cost"

    @staticmethod
    def fn(a, y):
        '''
            Returns the cost calculation 1/2 * (y - a) ** 2
        '''
        pass

    @staticmethod
    def delta(z, a, y):
        '''
            Returns dC/dz, ie, the partial derivative of cost wrt output layer's z value
        '''
        return (a-y) * sigmoid_prime(z)


class CrossEntropyCost(object):
    
    @staticmethod
    def name():
        return "Cross Entropy Cost"
    
    @staticmethod
    def fn(a, y):
        '''
            Return the cost calculation ie, y*ln(a) + (1-y)*ln(1-a)
        '''
        pass

    @staticmethod
    def delta(z, a, y):
        '''
            Return dC/dz ie, the partial derivative of cost wrt output layer's z value
        '''
        return (a-y)



class Network(object):
    
    def __init__(self, sizes, init_type, cost=CrossEntropyCost):
        #understood
        self.num_layers = len(sizes)
        self.bias_and_weight_initializer(sizes, init_type)
        #self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
        #self.weights = [ np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:]) ]
        self.cost = cost
        

    def bias_and_weight_initializer(self, sizes, init_type='default'):
        if init_type == 'default':
            self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
            self.weights = [ np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:]) ]
        elif init_type == 'He':
            self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
            self.weights = [ np.random.randn(y, x) * 2/np.sqrt(x) for x, y in zip(sizes[:-1], sizes[1:]) ]
        elif init_type == 'small':
            self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
            self.weights = [ np.random.randn(y, x) * 1/np.sqrt(x) for x, y in zip(sizes[:-1], sizes[1:]) ]
        else:
            print("Wrong initialization type provided")

        return
        

    def SGD(self, training_data, epochs, mini_batch_size, eta, test_data=None):
        '''
            Pick mini batches of data and update the weight vectors based on back prop done on
            each example in the mini batch.
        '''
        if test_data:
            n_test = len(test_data)

        print('l', len(training_data), len(training_data[0]), len(training_data[0][0]), len(training_data[0][1]))
        a, b = 0, 0
        epoch_list, costs = [], []
        for j in xrange(epochs):
            a = datetime.datetime.now()
            random.shuffle(training_data)
            mini_batches = [ training_data[k: k + mini_batch_size] for k in xrange(0, len(training_data), mini_batch_size) ]

            for mini_batch in mini_batches:
				self.update_mini_batch_vectorized_loop(mini_batch, eta)

            b = datetime.datetime.now()
            print(b - a)
            a = b
            if test_data:
               no_of_correct_answers = self.evaluate(test_data)
               print("Epoch %d completed with test data. Score %d / %d " %(j, no_of_correct_answers, n_test))
            else:
               print("Epoch %d done" %j)

            epoch_list.append(j)
            costs.append(no_of_correct_answers/float(n_test))
            plt.clf()
            plt.plot(epoch_list, costs)
            plt.pause(1.05)

        title = str(epochs) + " Epochs with 30 Hidden Neurons " + (self.cost).name()
        plt.title(title, fontname='Ubuntu', fontsize=14, fontweight='bold', color='green')

        filename = title + '.png'
        plt.savefig(filename)


    def update_mini_batch_vectorized_loop(self, mini_batch, eta):
        m = len(mini_batch)
        n_x = len(mini_batch[0][0])
        n_y = len(mini_batch[0][1])
        L = self.num_layers

        X = np.zeros((n_x, m))
        Y = np.zeros((n_y, m))
        i = 0
        for x, y in mini_batch:
            X[:, i] = np.squeeze(x) # to change x.shape from(n_x, 1) to (n_x,) - there might be a better way to do this
            Y[:, i] = np.squeeze(y) # to change y.shape from(n_y, 1) to (n_y,) - there might be a better way to do this
            i += 1

        nabla_b = [ np.zeros(b.shape) for b in self.biases]
        nabla_w = [ np.zeros(w.shape) for w in self.weights]

        # 1. Forward Pass
        A, Z = {}, {}
        A[0] = X
        for i in range(1, self.num_layers):
            Z[i] = np.dot(self.weights[i-1], A[i-1]) + self.biases[i-1]
            A[i] = sigmoid(Z[i])

        delta, nabla_b, nabla_w = {}, {}, {}
        nabla_b_array, nabla_w_array = [None] * len(self.biases), [None] * len(self.weights)

        # 2. Getting the 'error' from the 'final' layer

        delta[(L-1)] = (self.cost).delta(Z[L-1], A[L-1], Y)

        nabla_b[(L - 1)] = np.sum(delta[(L - 1)], axis=1, keepdims=True)
        nabla_w[(L - 1)] = np.dot(delta[(L - 1)], A[(L - 2)].T)

        # This is for indexing nabla_b_array and nabla_w_array. They are indexed a little differently.
        # They are similar to self.biases and self.weights in terms of indexing
        index_arrays = 1

        nabla_w_array[-index_arrays] = nabla_w[L-1]
        nabla_b_array[-index_arrays] = nabla_b[L-1]

        # 3. Backward pass throught subsequent layers
        for i in range(L-2, 0, -1):
            index_arrays += 1
            sp = sigmoid_prime(Z[i])
            delta[i] = np.dot(self.weights[i].T, delta[i+1]) * sp
            nabla_b[i] = np.sum(delta[i], axis=1, keepdims=True)
            nabla_w[i] = np.dot(delta[i], A[i-1].T)
            
            nabla_b_array[-index_arrays] = nabla_b[i]
            nabla_w_array[-index_arrays] = nabla_w[i]

        self.weights = [w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_w_array) ]
        self.biases = [b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_b_array) ]
        return

    
    def feedforward(self, a):
        '''
            given the input a, this function outputs the networks output, ie, the network's y
            with its biases and weights. Usually called after the learning is completed.
        '''
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a) + b)
        return a


    def evaluate(self, test_data):
        '''
            test data contains x and y,
            Evaluate the output from the network (after the network has learned)
        '''
        #test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in test_data]
        #return sum(int(x == y) for (x, y) in test_results)
        success = 0
        for x, y in test_data:
            network_output = np.argmax(self.feedforward(x))
            if y == network_output:
                success += 1
        return success
                

def sigmoid(z):
    return 1.0 / ( 1 + np.exp(-z) )



def sigmoid_prime(z):
    return sigmoid(z) * ( 1 - sigmoid(z) )
