import random
import numpy as np

class Network(object):
    
    def __init__(self, sizes):
        # sizes = number of neurons in each layer, if [3, 4, 5] it means 3rd in the 1st,
        # 4 in the 2nd and 5 in the 3rd layer.

        self.num_layers = len(sizes)

        # create biases for all neurons except those in the first layer.
        # biases for all neurons in the first layer will be in self.biases[l]
        self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]

        # zip(sizes[:-1], sizes[1:]) will give you [(3, 4), (4, 5)]. It becomes easier
        # to represent weights this way. We generate matrices of size (3, 4) and (4, 5).
        # to depict weights connecting layer  (1 and 2) and layer (2 and 3).

        # NOTE that it is not np.random.randn(x, y) but instead np.random.randn(y, x).
        # self.weights[j][k] means weight connecting neuron k in the (l-1)th layer
        # and neuron j in the (l)th layer. This kind of representation makes
        # computational notations much more convenient.
        # w[l] represents the weights coming in from the (l)th layer, it is a little
        # unnatural because the natural way of thinking about w[l] is weights going out of
        # (l)th layer.

        # we can compute output z as z = w*a + b in this form. Otherwise we should use
        # z as z = transpose(w)*a + b. I have taken notes for this and actually understood this
        # very well.
        self.weights = [ np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:] ) ]


    def feedforward(self, a):
        # given the input to the network as variable 'a', it outputs the output of the
        # networks by iteratively computing activations in each respective layers.
        # Something like this is really easy to think about and code if you are 
        # comfortable with thining in a vectorized way - I think. I remember spending
        # a lot of time thinking about whether the dot product will actually work
        # while doing the LFD class.

        for w, b in zip(self.weights, self.biases):
            a = sigmoid(np.dot(w, a) + b);
        return a

    def SGD(self, training_data, epochs, mini_batch_size, eta, test_data=None):
        '''
            Stochastic Gradient Descent.
            for number of epochs
                split the training_data into mini_batches
                for each minibatch
                    forward pass
                    compute error and propagate them backwards and upate weights ie, backward pass
                if test_data is provided
                    evaluate the result 

            training_data = (x,y) x is input and y is output
            eta = learning rate,
            others are self explanatory
        '''
        if test_data:
            n_test = len(test_data)

        for j in xrange(epochs):
            random.shuffle(training_data)
            # nielsen didn't put len(training_data) inside the for, he instead initialized n = len(training_data)
            # and used that.
            mini_batches = [ training_data[k: k+ mini_batch_size] for k in xrange(0, len(training_data), mini_batch_size) ]

            for mini_batch in mini_batches:
                self.update_mini_batch(mini_batch, eta)

            if test_data:
                print "Emils Epoch {0}: {1} / {2}".format(j, self.evaluate(test_data), n_test)
                print "Emils Epoch {0}: {1} / {2}".format(j, self.evaluate_real(test_data), n_test)
            else:
                print "Emils Epoch {0} complete".format(j)
                
        
    def update_mini_batch(self, mini_batch, eta):
        '''
            Get one minibatch, does backpropagation in it and updates the weights and biases

            NOTE: I HAVEN'T FULLY UNDERSTOOD THE DETAILS OF THIS FUNCTION AND 
            THE BACKPROPAGATION
        '''
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        for x, y in mini_batch:
            # for each example, backprop gives you the partial derivative of C wrt to weights and biases.
            # we add them up and compute the sum of the gradients of all training examples in the next two lines.
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)

            # here we are summing the product (product is computed by backprop)
            # zip is used so that the summing can be written elegantly.
            # all it is doing is go through the delta_nabla_b for each training example
            # and add it to nabla_b and nabla_w
            nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]

        self.weights = [w-(eta/len(mini_batch))*nw for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b-(eta/len(mini_batch))*nb for b, nb in zip(self.biases, nabla_b)]


    def backprop(self, x, y):
        # computes backpropation for one single training case (input)

        """Return a tuple "(nabla_b, nabla_w)" representing the
        gradient for the cost function C_x.  "nabla_b" and
        "nabla_w" are layer-by-layer lists of numpy arrays, similar
        to "self.biases" and "self.weights".""" 

        # I have a holistic idea, but am not entirely clear with the details.
        # I can do the algorithm, but not code this own my own yet.

        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        #1. Feedforward
        activation = x
        activations = [x] # intialize the input actions as input itself.
        zs = [] # list to store all z vectors, layer by layer

        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)

        #2. Compute the output error
        delta = self.cost_derivative(activations[-1], y) * sigmoid_prime(zs[-1])
        
        # update the last layer nabla_w and nabla_b
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose()) # activations[-2] because activations[-1] is actually y.

        #3. backpropagate the error delta
        # l here is used to index from the last item via -l
        for l in xrange(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z) # find derivative of activation function wrt z
            delta = np.dot(self.weights[-l + 1].transpose() , delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l - 1].transpose())

        return (nabla_b, nabla_w)


    def cost_derivative(self, output_activations, y):
        # Returns partial C/ partial a
        # I understood this.
        return (output_activations - y)

    def evaluate(self, test_data):
        '''
            self.feedforward will return the output activations.
            Among all the output activations the index that has the maximum value
            is considered the output of the neuron. 
            For example, if among all the 10 output activations, the 8th one has the
            maximum value, then the output is 8.
        '''
        success = 0
        for x, y in test_data:
            network_output = np.argmax(self.feedforward(x))
            if network_output == y:
                success += 1
        return success

    def evaluate_real(self, test_data):
        '''
            This is the original evaluation function by Michael, It has two for loops
            but the one that I wrote above has only one and mine looks neat and straightforward.
        '''
        test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in test_data]
        return sum(int(x == y) for (x, y) in test_results)
        
        
def sigmoid(z):
    '''
        return 1.0 / ( 1.0 + np.exp(-z) )
    '''
    return 1.0 / ( 1.0 + np.e**(-z) )

def sigmoid_prime(z):
    '''
        Return the derivative of the sigmoid function
    '''
    return sigmoid(z) * (1 - sigmoid(z))
