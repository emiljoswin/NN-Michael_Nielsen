'''
    Attempting to combine both MN and Andrew NG
'''

import numpy as np

def sigmoid(Z):
	'''
		Input:
			Z - np array of z values of the layer
		Working:
			Computes the sigmoid activation function
		Returns:
			A - activation of the layer
	'''
	A = 1.0 / ( 1 + np.exp(-z) )
	cache = Z
	return A, cache


def relu(Z):
	'''
		Input:
			Z - np array of z values of the layer
		Working:
			Computes the relu activation function
		Returns:
			A - activation of the layer
	'''
	A = np.maximum(Z, 0)
	cache = Z
	return A, cache

def initialize_parameters_vectorized(layer_dimensions):
    '''
		layer_dimensions -- array of dimensions
		eg: [5, 4, 3] -- 5 neurons in first layer, 4 in second layer and 3 in third layer

		returns:
			Weight and bias matrices in vectorized form after initialization
    '''

	parameters = {}
	L = len(layer_dimensions)

	for l in range(1, L):
		parameters["W" + str(l)] = np.random.randn( layer_dimensions[l], layer_dimensions[l-1] )
		# OPTIMIZED 
		# Ro make sure weights are as small as possible
		# parameters["W" + str(l)] = np.random.randn( layer_dimensions[l], layer_dimensions[l-1] ) * 0.01

		parameters["b" + str(l)] = np.zeros( (layer_dimensions[l] , 1) )

	return parameters


def linear_forward(A, W, b):
	'''
		Implement the linear forward pass of a single layer of the network ie, sigmoid(w*a + b).
		
		result:
			z - ie, the output from the layer before it is passed through activation function
	'''

	Z = np.dot(W, A) + b
	cache = (A, W, b) #don't understand yet what this is used for

	return Z, cache


def linear_activation_function(A_prev, W, b, activation):
	'''
		Input:
			A_prev 		-- previous layer's output, 
			W 	   		-- current layers weights
			b      		-- current layers biases
			activation 	-- "sigmoid"/"relu" etc

		Working:
			Calls the function 'linear_forward' to compute Z and then 
			computes the activation of the layer depending on the 
			activation function provided
	'''

	Z, linear_cache  = linear_forward(A_prev, W, b)

	if activation == 'sigmoid':
		A, activation_cache = sigmoid(Z)

	elif activation == 'relu':
		A, activation_cache = relu(Z)
	
	cache = (linear_cache, activation_cache)
	return A, cache


def L_model_forward(X, parameters):
	'''
		Input:
			X 			-- input 
			parameters 	-- initial weights and biases
		Working:
			Does linear forward propagation of the NN.
		Output:
			AL 			-- the last layer's activation, which you may intrepret
			as the output from the network
	'''

	caches = [] # to store caches of forward propagation

	L = len(parameters) / 2 # gives the total number of layers in the network

	A = X

	for l in range(1, L):
		A_prev = A
		A, cache = linear_activation_function(A_prev, parameters["W" + str(l)], parameters["b" + str(l)], activation="sigmoid")
		caches.append(cache)

	# computing activation of the last layer
	AL, cache = linear_activation_function(A, parameters["W" + str(L)], parameters["b" + str(L)], activation="sigmoid")
	caches.append(cache)

	return AL, cache


def compute_cost(AL, Y):
	'''
		Input:
			AL 		-- activation of the last layer ie, output of the network
			Y 		-- actual value of the output (from the training data)

		Working:
			Calculates the cross entropy cost

		Output:
			cost 	--  calculates the cross entropy cost of the system
	'''
	m = Y.shape[1] # the total number of training examples
	
	cost = np.sum( np.multiply( Y, np.log(AL) ) + np.multiply( 1-Y, np.log(1-AL) ) ) * -1/m

	return cost


def linear_backward(dZ, cache):
	'''
		Input:
			dZ 	-- derivative of the cost with respect to the output(Z) of the layer

		Working:
			Calculates the backward propagation in one single layer.
			It calculates the derivative of the cost wrt to the wights and biases of that layer.

	'''
	A_prev, W, b = cache
	m = A_prev.shape[1] # no of examples

	dW = 1/m * np.dot(dZ, A_prev.T)
	db = 1/m * np.sum(dZ, axis=1, keepdims=True)
	dA_prev = np.dot(W.T, dZ)

	return dA_prev, dW, db


def linear_activation_backward(dA, cache, activation):
	'''
		TODO
	'''

	linear_cache, activation_cache = cache

	if activation == 'relu':
