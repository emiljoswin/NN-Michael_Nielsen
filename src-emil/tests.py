import numpy as np
import network_cost_function
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

style.use('fivethirtyeight')

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def test_single_neuron():
    # This is done to illustrate the learning characteristics of a single neuron. From observing these charactaristics,
    # we'll learn about how to improve backpropagation.

    # There is only a single neuron here which should learn to output 0 when given 1.
    # The test data and training data are the same.
    t_data = ( (np.array(1), np.array(0)), )
    weights = np.array([np.array([[2.0]])])
    biases = np.array([np.array([[2.0]])])

    # initialize the neural network. It contains one single neuron and one single input neuron.

    net = network_cost_function.Network([1, 1], weights, biases)

    #start learning 
    mini_batch_size = 1 # there is only a single input
    epochs = 300
    eta = 0.15

    #(xs, ys) = net.SGD(t_data, epochs, mini_batch_size, eta, 'quadratic_cost', test_data=t_data, graph=True)
    #plot_shit(xs, ys, 'quadratic_cost')

    (xs, ys) = net.SGD(t_data, epochs, mini_batch_size, eta, 'cross_entropy_cost', test_data=t_data, graph=True)
    plot_shit(xs, ys, 'cross_entropy_cost')

 
def plot_shit(xs, ys, cost_function_name):
    ax1.clear()
    ax1.plot(xs, ys)
    plt.xlabel("Epochs")
    plt.ylabel("Cost")
    fig.suptitle(cost_function_name)
    plt.show()

    
    
