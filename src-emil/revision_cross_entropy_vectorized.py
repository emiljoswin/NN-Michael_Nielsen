# Trying my hand once more at this. I has almost quit doing this, before the
# sudden revival of interest.

## TODO - 25/09/2017, 
## 1. convert the backprop through layers into a loop. This makes it much more readable
## 2. Have different classes for different cost functions, a) quadartic cost, b) cross entropy, c) loglikelihood
## 3. Have a mechanism that will enable the plugging in of different activaion functions for different layers.

import numpy as np
import random
import datetime
import matplotlib.pyplot as plt
from matplotlib import style

style.use('fivethirtyeight')

plt.ion()
plt.xlabel('Epochs')
plt.ylabel("Cost")

class Network(object):
    
    def __init__(self, sizes):
        #understood
        self.num_layers = len(sizes)
        self.biases = [ np.random.randn(y, 1) for y in sizes[1:] ]
        self.weights = [ np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:]) ]

    def SGD(self, training_data, epochs, mini_batch_size, eta, test_data=None):
        '''
            Pick mini batches of data and update the weight vectors based on back prop done on
            each example in the mini batch.
        '''
        if test_data:
            n_test = len(test_data)

        print('l', len(training_data), len(training_data[0]), len(training_data[0][0]), len(training_data[0][1]))
        a, b = 0, 0
        epoch_list, costs = [], []
        for j in xrange(epochs):
            a = datetime.datetime.now()
            random.shuffle(training_data)
            mini_batches = [ training_data[k: k + mini_batch_size] for k in xrange(0, len(training_data), mini_batch_size) ]

            for mini_batch in mini_batches:
				#self.update_mini_batch_vectorized(mini_batch, eta)
				self.update_mini_batch_vectorized_loop(mini_batch, eta)

            b = datetime.datetime.now()
            print(b - a)
            a = b
            if test_data:
               no_of_correct_answers = self.evaluate(test_data)
               print("Epoch %d completed with test data. Score %d / %d " %(j, no_of_correct_answers, n_test))
            else:
               print("Epoch %d done" %j)

            epoch_list.append(j)
            costs.append(no_of_correct_answers/float(n_test))
            plt.clf()
            plt.plot(epoch_list, costs)
            plt.pause(1.05)

        plt.title("30 Epochs with 100 Hidden Neurons", fontname='Ubuntu', fontsize=14, fontweight='bold', color='green')
        plt.savefig('30epochs100hidden_sigmoid_cross_entropy.png')
        #while True:
        #    plt.pause(0.001)


    def update_mini_batch(self, mini_batch, eta):
        '''
            Update the full weight matrix for the network using one mini batch.
            Note that the gradients are summed up and only after the entire minibatch is
            returns the gradients, do we actually reduce the weights and biases
        '''
        #initialize zero arrays to keep the sum of the gradients returned from backprop.
        nabla_b = [ np.zeros(b.shape) for b in self.biases ]
        nabla_w = [ np.zeros(w.shape) for w in self.biases ]

        for x, y in mini_batch:
            delta_nabla_b, delta_nabla_w = self.backprop(x, y)


            nabla_b = [ nb + dnb for nb, dnb in zip(nabla_b, delta_nabla_b) ]
            nabla_w = [ nw + dnw for nw, dnw in zip(nabla_w, delta_nabla_w) ]

        # understood well. We are just doing w = w - eta*gradient.
        # It is however, done in this form because we are doing it for a bunch of training data
        # inside the mini_batch and not just one dealing with one single gradient.
        # We are summing the gradient for all the examples inside the minibatch
        self.weights = [ w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_w) ]
        self.biases= [ b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_b) ]

    def update_mini_batch_vectorized(self, mini_batch, eta):

        m = len(mini_batch)  # no of examples in the mini_batch
        n_x = len(mini_batch[0][0]) # n_x ie, the no of features of a single example
        n_y = len(mini_batch[0][1]) # n_y ie, the length of the y vector of a single example

        X = np.zeros((n_x, m))
        Y = np.zeros((n_y, m))
        i = 0
        for x, y in mini_batch:
            X[:, i] = np.squeeze(x) # to change x.shape from(n_x, 1) to (n_x,) - there might be a better way to do this
            Y[:, i] = np.squeeze(y) # to change y.shape from(n_y, 1) to (n_y,) - there might be a better way to do this
            i += 1

        #initialize zero arrays to keep the sum of the gradients returned from backprop.
        nabla_b = [ np.zeros(b.shape) for b in self.biases]
        nabla_w = [ np.zeros(w.shape) for w in self.weights]

        # 1. Forward pass
        A, Z = {}, {}
        A[0] = X
        #print('w', len(self.weights), 'w', self.weights[0].shape, 'w', self.weights[1].shape)
        Z[1] = np.dot(self.weights[0], X) + self.biases[0]
        A[1] = sigmoid(Z[1]) # make sure this is working properly
        #print('Z', Z[1].shape)
        #print('A', A[1].shape)
        Z[2] = np.dot(self.weights[1], A[1]) + self.biases[1]
        A[2] = sigmoid(Z[2])
        #print('Z', Z[2].shape)
        #print('A', A[2].shape)

        # 2. Backward pass
        delta, nabla_b, nabla_w = {}, {}, {}
        delta[1] =  self.cost_derivative(A[2], Y) # last layer
        #print("delta", delta[1].shape)
        nabla_b[1] = np.sum(delta[1], axis=1, keepdims=True) # I think biases has to be summed, weights gets summed automatically during matrix multiplication
        nabla_w[1] = np.dot(delta[1], A[1].T)
        #print('nabla_b', nabla_b[1].shape)
        #print('nabla_w', nabla_w[1].shape)
        sp = sigmoid_prime(Z[1])
        delta[0] = np.dot(self.weights[1].T, delta[1]) * sp
        #print('delta', delta[0].shape)
        nabla_b[0] = np.sum(delta[0], axis=1, keepdims=True) # I think biases has to be summed, weights gets summed automatically during matrix multiplication
        nabla_w[0] = np.dot(delta[0], A[0].T)
        #print('nabla_b', nabla_b[0].shape)
        #print('nabla_w', nabla_w[0].shape)


        nabla_b_array = [nabla_b[0], nabla_b[1]]
        nabla_w_array = [nabla_w[0], nabla_w[1]]
        #print('b', len(nabla_b_array), len(nabla_b_array[0]), len(nabla_b_array[1]))
        #print(nabla_b_array[0].shape)

        #print('2', len(nabla_w_array), len(nabla_w_array[0]), len(nabla_w_array[1]))
        #print(nabla_w_array[0].shape, nabla_w_array[1].shape)

        # Update the weights
        self.weights = [ w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_w_array) ]
        self.biases= [ b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_b_array) ]

        return (nabla_b_array, nabla_w_array)

    def update_mini_batch_vectorized_loop(self, mini_batch, eta):
        m = len(mini_batch)
        n_x = len(mini_batch[0][0])
        n_y = len(mini_batch[0][1])
        L = self.num_layers

        X = np.zeros((n_x, m))
        Y = np.zeros((n_y, m))
        i = 0
        for x, y in mini_batch:
            X[:, i] = np.squeeze(x) # to change x.shape from(n_x, 1) to (n_x,) - there might be a better way to do this
            Y[:, i] = np.squeeze(y) # to change y.shape from(n_y, 1) to (n_y,) - there might be a better way to do this
            i += 1

        nabla_b = [ np.zeros(b.shape) for b in self.biases]
        nabla_w = [ np.zeros(w.shape) for w in self.weights]

        # 1. Forward Pass
        A, Z = {}, {}
        A[0] = X
        for i in range(1, self.num_layers):
            Z[i] = np.dot(self.weights[i-1], A[i-1]) + self.biases[i-1]
            A[i] = sigmoid(Z[i])

        delta, nabla_b, nabla_w = {}, {}, {}
        nabla_b_array, nabla_w_array = [None] * len(self.biases), [None] * len(self.weights)

        # 2. Getting the 'error' from the 'final' layer
        delta[(L - 1)] = self.cost_derivative(A[L - 1], Y)
        nabla_b[(L - 1)] = np.sum(delta[(L - 1)], axis=1, keepdims=True)
        nabla_w[(L - 1)] = np.dot(delta[(L - 1)], A[(L - 2)].T)

        # This is for indexing nabla_b_array and nabla_w_array. The are indexed a little differently.
        # They are similar to self.biases and self.weights in terms of indexing
        index_arrays = 1

        nabla_w_array[-index_arrays] = nabla_w[L-1]
        nabla_b_array[-index_arrays] = nabla_b[L-1]

        # 3. Backward pass throught subsequent layers
        for i in range(L-2, 0, -1):
            index_arrays += 1
            sp = sigmoid_prime(Z[i])
            delta[i] = np.dot(self.weights[i].T, delta[i+1]) * sp
            nabla_b[i] = np.sum(delta[i], axis=1, keepdims=True)
            nabla_w[i] = np.dot(delta[i], A[i-1].T)
            
            nabla_b_array[-index_arrays] = nabla_b[i]
            nabla_w_array[-index_arrays] = nabla_w[i]

        self.weights = [w - (eta/len(mini_batch)) * nw for w, nw in zip(self.weights, nabla_w_array) ]
        self.biases = [b - (eta/len(mini_batch)) * nb for b, nb in zip(self.biases, nabla_b_array) ]
        return

    def backprop(self, x, y):
        nabla_b = [ np.zeros(b.shape) for b in self.biases ]
        nabla_w = [ np.zeros(w.shape) for w in self.weights ]

        # 1. forward pass
        activation = x
        activations = [x] #in first layer the input is the activation
        zs = [] #array to store all z vectors, layer by layer

        #for w, b in zip(self.weights, self.biases):
        for b, w in zip(self.biases, self.weights):
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = sigmoid(z)
            activations.append(activation)

        # 2. Compute the gradient of the cost
        # NOTE: Need not use a funciton to calculate derivative of cross entropy function, Instead just omit sigmoid_prime.
        # it is going to get cancelled in the equation.
        delta = self.cost_derivative(activations[-1], y)

        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, activations[-2].transpose()) #activations[-1] is the output of network, ie, network's y, ie, the networks last layer.

        # 3. Backward pass
        # l is used to index from the back of the layer by going -l, -l-1, -l-2 etc 
        for l in xrange(2, self.num_layers):
            z = zs[-l]
            sp = sigmoid_prime(z)
            delta = np.dot(self.weights[-l+1].transpose(), delta) * sp
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())

        return (nabla_b, nabla_w)

    def cost_derivative(self, output_activations, y):
        '''
            cost derivative of quadratic cost
        '''
        return (output_activations - y)

    def cost_derivative_cross_entropy(self, output_activations, y):
        '''
            Cost derivative of cross entropy cost.
            If we use this cost function, we can make the learning speed proportional
            to the magnitude of the error ie, the more wrong we are the faster we learn
            to correct it. That is how humans learn, we realize the wrongness faster when we are 
            more wrong at something rather than being less wrong at it. Sigmoid activation  with
            cross entropy cost has this problem and the reason is stated below.

            The error that gets calculated at the output layer is asymptoted by the derivative 
            of the activation function like this:

                delta = derivative of C wrt a * derivative of a wrt z

            What happens here is the activation saturates quickly to both positive and negative ends (property of sigmoid function - check its graph).
            Hence, the derivative will be close to zero and delta will be close to zero and therefore
            the learning happens really slowly.
            It is quite clever how this can be mitigated. My first impression was to change the activation
            function to something that has different properties. 
            Instead what they do is change the cost function in such a way that that some parts of the 
            derivative of the Cost wrt a will cancel out the derivative of activation function entirely.
                
                delta = "some term that doesnt have derivative of activation function"

            The cross entropy function when differentiated will have a denominator equal to the
            derivative of the objective function and they gets cancelled out.
        '''
        return (output_activations - y) / (output_activations * (1 - output_activations))

    def feedforward(self, a):
        '''
            given the input a, this function outputs the networks output, ie, the network's y
            with its biases and weights. Usually called after the learning is completed.
        '''
        for b, w in zip(self.biases, self.weights):
            a = sigmoid(np.dot(w, a) + b)
        return a

    def evaluate(self, test_data):
        '''
            test data contains x and y,
            Evaluate the output from the network (after the network has learned)
        '''
        #test_results = [(np.argmax(self.feedforward(x)), y) for (x, y) in test_data]
        #return sum(int(x == y) for (x, y) in test_results)
        success = 0
        for x, y in test_data:
            network_output = np.argmax(self.feedforward(x))
            if y == network_output:
                success += 1
        return success
                


def sigmoid(z):
    return 1.0 / ( 1 + np.exp(-z) )

def sigmoid_prime(z):
    return sigmoid(z) * ( 1 - sigmoid(z) )
